# Películas App

>Películas App, by José Antonio Cuenca, **fullstack JavaScript developer** and **fourth-sector entrepreneur** based on Barcelona. Check my [Linkedin profile](www.linkedin.com/in/josé-antonio-cuenca-9b085935)

Exercises App it's an application for training purposes. It integrates:
- Bootstrap 4
- ngPrime
- JSONP


## Roadmap

**Películas App** is still under deployment. These are the next features to be released:
- Many code improvements such as error management scaffold
- Include tests

# Other projects

Check [Exercises App repo](https://bitbucket.org/qnk/exercises-app) to find my code with:
- Angular Material
- FlexLayout
