import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {
  onTheaters;
  popular;

  private onTheatersEndOfSubscription = null;
  private popularEndOfSubscription = null;

  constructor(private moviesService: MoviesService) { }

  ngOnInit() {
    this.moviesService.onTheaters().subscribe((movies) => {
      this.onTheaters = movies.results;
    });

    this.moviesService.popular().subscribe((movies) => {
      this.popular = movies.results;
    });
  }

}
