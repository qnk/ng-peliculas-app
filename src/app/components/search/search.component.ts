import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { ActivatedRoute } from '@angular/router';
import { NotFoundError } from '../../common/not-found-error';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent implements OnInit {
  public query: string = "";
  public moviesFound;
  public searched: boolean = false;

  constructor(
    private moviesService: MoviesService,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(params => {
      // console.log(params);
    });
  }

  ngOnInit() {
  }

  searchFilm() {
    if(this.query.length === 0) {
      this.searched = false;
      return;
    }

    this.moviesService.findByQuery(this.query).subscribe(
      results => {
        this.searched = true;
        this.moviesFound = results;
      },
      (error: Response) => {
        if(error instanceof NotFoundError) {
          alert('Not found');
        }
    });
  }

}
