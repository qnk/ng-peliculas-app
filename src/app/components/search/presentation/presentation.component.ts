import { Component, OnInit, Input } from '@angular/core';

import { TableModule } from 'primeng/table';

@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styles: []
})
export class PresentationComponent implements OnInit {
  @Input('movies') movies;
  @Input('title') title;

  constructor() { }

  ngOnInit() {
  }

}
