import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { JsonpModule, Jsonp, Response } from '@angular/http';

import { AppComponent } from './app.component';
import { MoviesService } from './services/movies.service';
import { PeliculasComponent } from './components/peliculas/peliculas.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { MovieComponent } from './components/movie/movie.component';
import { SearchComponent } from './components/search/search.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { APP_ROUTING } from './app.routes';
import { TableModule } from 'primeng/table';
import { PresentationComponent } from './components/search/presentation/presentation.component';

@NgModule({
  declarations: [
    AppComponent,
    PeliculasComponent,
    NavbarComponent,
    HomeComponent,
    MovieComponent,
    SearchComponent,
    PresentationComponent
  ],
  imports: [
    BrowserModule,
    JsonpModule,
    TableModule,
    FormsModule,
    APP_ROUTING
  ],
  providers: [
    MoviesService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
