import { Injectable } from '@angular/core';
import { Jsonp } from "@angular/http";

import { Observable } from 'rxjs/Observable';

import 'rxjs/Rx'; // Map
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import * as moment from 'moment';
import { AppError } from '../common/app-error';

@Injectable()
export class MoviesService {

  private apikey:string = "fdf6f5a3a0b311fb492c2fb7dd904c0a";
  private urlMoviedb:string = "https://api.themoviedb.org/3";

  constructor( private jsonp:Jsonp ) { }

  onTheaters() {
    const fromDate = moment().dayOfYear(1).format('YYYY-DD-MM');
    const toDate = moment().format('YYYY-DD-MM');

    const url = `${this.urlMoviedb}/discover/movie?primary_release_date.gte=${fromDate}&primary_release_date.lte=${fromDate}&api_key=${ this.apikey }&language=es&callback=JSONP_CALLBACK`;

    return this.jsonp.get(url).map( res=> res.json() );
  }

  popular() {
    const url = `${this.urlMoviedb}/discover/movie?sort_by=popularity.desc&api_key=${this.apikey}&language=es&callback=JSONP_CALLBACK`;

    return this.jsonp.get(url).map( res=> res.json() );
  }

  findByQuery( texto:string ){
    const url = `${this.urlMoviedb}/search/movie?query=${texto}&sort_by=popularity.desc&api_key=${this.apikey}&language=es&callback=JSONP_CALLBACK`;

    return this.jsonp.get( url ).catch((error: Response) => {
      return Observable.throw(new AppError(error));
    }).map( res=> res.json().results);
  }

}
